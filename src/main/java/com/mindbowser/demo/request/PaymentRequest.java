package com.mindbowser.demo.request;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class PaymentRequest {

	double amount;

	long cardNumber;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/yy")
	String expiryDate;

	int cvv;

}
